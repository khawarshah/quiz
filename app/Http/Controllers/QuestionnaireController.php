<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use Auth;
use DB;


class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $questionnaires =  Questionnaire::with("questions")->where("created_by","=", Auth::id())->get();
        return view("questionnaire.index", ['questionnaires' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   

        return view("questionnaire.create");

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'duration' => 'required',
            'duration_in' => 'required',
            'is_resumeable' => 'required'
        ]);
        $request->request->add(['created_by' => Auth::id()]);
        if(Questionnaire::create($request->all())){
            // return response()->json("created Sucessfully");
            return redirect()->route("questionnaires");

        }
        return redirect()->route("questionnaires");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'duration' => 'required',
            'duration_in' => 'required',
            'is_resumeable' => 'required'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $remove = Questionnaire::where("id", "=", $id)->where("created_by", "=", Auth::id())->delete();
        if ($remove) {
            return redirect()->route("questionnaires");            
        }
        return response()->json("Something went wrong");
    }
}
