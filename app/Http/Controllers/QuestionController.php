<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Answer;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->input("id");
        return view("questions.create", ["id" => $id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // return $request->all();
        $questionnaire_id = $request->input("questionnaire_id");
        $quesions = $request->input("questions");
        foreach ($quesions as $value) {
            $question = Question::create([
                'question' => $value['question'],
                'question_type' => $value['type'],
                'questionnaire_id' => $questionnaire_id
            ]);
            foreach ($value['answers'] as $v) {
                if (isset($v["status"])) {
                    $status = $v["status"];
                } else{
                    $status = 0;                    
                }
                $answer = $v["answer"];
                Answer::create([
                    'question_id' => $question->id,
                    'answer' => $answer,
                    'is_correct' => $status 
                ]);
            }
        }
        return redirect()->route("questionnaires");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questions = Question::with("answers")->where("questions.questionnaire_id", "=", $id)->get();
        return view("questions.index", ['questions' => $questions]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
