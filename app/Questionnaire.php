<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
   	protected $table = "questionnaires_type";
   	protected $fillable = ["name", "duration", "duration_in", "is_resumeable", "created_by"];


   	public function user(){
   		return  $this->belongsTo("App\User", "created_by");
   	}

   	public function questions(){
   		return  $this->hasMany("App\Question", "questionnaire_id");
   	}   	
}
