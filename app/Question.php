<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = "questions";
    protected $fillable = ["question", "question_type", "questionnaire_id"];



    public function answers(){
    	return $this->hasMany("App\Answer", "question_id");
    }
}
