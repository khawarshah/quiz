<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['middleware' => 'auth'], function(){

	Route::get("questionnaires", [
		'uses' => 'QuestionnaireController@index',
		'as' => 'questionnaires'
	]);

	Route::post("questionnaires/create", [
		'uses' => 'QuestionnaireController@store',
		'as' => 'questionnaires.create'
	]);

	Route::get("questionnaires/create", [
		'uses' => 'QuestionnaireController@create',
		'as' => 'questionnaires.create'
	]);

	Route::delete("questionnaire/{id}", [
		'uses' => 'QuestionnaireController@destroy',
		'as' => 'questionnaire'
	]);

	Route::post("question/add", [
		'uses' => 'QuestionController@create',
		'as' => 'question.add'
	]);

	Route::post("question/create", [
		'uses' => 'QuestionController@store',
		'as' => 'question.create'
	]);


	Route::get("questionnaires/list/{id}", [
		'uses' => 'QuestionController@show',
		'as' => 'questionnaires.list'
	]);



});

Route::get('/home', 'HomeController@index')->name('home');
