-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `answer` text NOT NULL,
  `is_correct` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `question_id` (`question_id`),
  CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `answers` (`id`, `question_id`, `answer`, `is_correct`, `created_at`, `updated_at`) VALUES
(13,	11,	'ng-model directive binds the values of AngularJS application data to HTML input controls.',	0,	'2017-08-18 11:58:18',	'2017-08-18 11:58:18'),
(14,	11,	'ng-model directive creates a model variable which can be used with the html page and within the container control having ng-app directive.',	0,	'2017-08-18 11:58:18',	'2017-08-18 11:58:18'),
(15,	11,	'Both of the above.',	1,	'2017-08-18 11:58:18',	'2017-08-18 11:58:18'),
(16,	11,	'None of the above.   Show Answer',	0,	'2017-08-18 11:58:18',	'2017-08-18 11:58:18'),
(17,	12,	'True',	1,	'2017-08-18 11:58:19',	'2017-08-18 11:58:19'),
(18,	12,	'False',	0,	'2017-08-18 11:58:19',	'2017-08-18 11:58:19'),
(19,	13,	'filter filter is a function which takes text as input.',	0,	'2017-08-18 12:16:42',	'2017-08-18 12:16:42'),
(20,	13,	'filter filter is used to filter the array to a subset of it based on provided criteria.',	1,	'2017-08-18 12:16:42',	'2017-08-18 12:16:42'),
(21,	13,	'Both of the above.',	0,	'2017-08-18 12:16:42',	'2017-08-18 12:16:42'),
(22,	13,	'None of the above.   Show Answer',	0,	'2017-08-18 12:16:42',	'2017-08-18 12:16:42'),
(23,	15,	'Lumen Answer 1',	0,	'2017-08-18 12:26:27',	'2017-08-18 12:26:27'),
(24,	15,	'Lumen Answer 2',	1,	'2017-08-18 12:26:28',	'2017-08-18 12:26:28'),
(25,	15,	'Lumen Answer 3',	1,	'2017-08-18 12:26:28',	'2017-08-18 12:26:28'),
(26,	15,	'Lumen Answer 4',	0,	'2017-08-18 12:26:28',	'2017-08-18 12:26:28');

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1);

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `questionnaires_type`;
CREATE TABLE `questionnaires_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `duration` varchar(100) NOT NULL,
  `duration_in` enum('min','hr') NOT NULL,
  `is_resumeable` enum('Yes','No') NOT NULL DEFAULT 'No',
  `is_published` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created_by` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `questionnaires_type_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `questionnaires_type_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `questionnaires_type` (`id`, `name`, `duration`, `duration_in`, `is_resumeable`, `is_published`, `created_by`, `created_at`, `updated_at`) VALUES
(1,	'Angular',	'1',	'hr',	'No',	'No',	1,	'2017-08-17 11:09:06',	'2017-08-17 11:09:06'),
(2,	'lumen',	'2',	'hr',	'No',	'No',	1,	'2017-08-17 14:24:40',	'2017-08-17 14:24:40');

DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `question_type` varchar(255) NOT NULL,
  `questionnaire_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `questionnaire_id` (`questionnaire_id`),
  CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`questionnaire_id`) REFERENCES `questionnaires_type` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `questions` (`id`, `question`, `question_type`, `questionnaire_id`, `created_at`, `updated_at`) VALUES
(11,	'Which of the following is true about ng-model directive?',	'MCQs',	1,	'2017-08-18 11:58:18',	'2017-08-18 11:58:18'),
(12,	'AngularJS application expressions are pure JavaScript expressions.',	'MCQs',	1,	'2017-08-18 11:58:19',	'2017-08-18 11:58:19'),
(13,	'Which of the following is true about filter filter?',	'MCQs',	1,	'2017-08-18 12:16:41',	'2017-08-18 12:16:41'),
(15,	'What is Lumen?',	'MCQs',	2,	'2017-08-18 12:26:27',	'2017-08-18 12:26:27');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100)  NOT NULL,
  `email` varchar(100)  NOT NULL,
  `password` varchar(100)  NOT NULL,
  `remember_token` varchar(100)  DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'khawar',	'khawarshah06@gmail.com',	'$2y$10$YHh4Tvt00x/xF1nRvdYkKOC4vTu9cdTA.6WMpBsz/xLjoh4E9J8iC',	'YAaKpAezVftvIaFIW9Ql6LyBUfyG3Dd7HaaYHsBHTOhEDY2Ww0M27ry2YcvG',	'2017-08-17 09:56:05',	'2017-08-17 09:56:05');

-- 2017-08-18 18:01:47
