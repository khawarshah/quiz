@extends('layouts.app')
@section('content')
<div class="container">
	<h1>Questions List</h1>
	@foreach($questions as $question)
	<h4>Q: {{$question->question}}</h4>
	<ul>
		@foreach($question->answers as $answer)
			<li style="color: {{ $answer->is_correct == 1 ? 'green' : 'black' }};">{{ $answer->answer }}</li>
		@endforeach
	</ul>
	<hr>
	@endforeach


</div>

@endsection