@extends('layouts.app')
@section('content')
<div class="container">
	<h1>Add Question</h1>

	{{ Form::open(["route" => "question.create", "class" => "form-horizontal", "method" => "POST"]) }}
		<input type="hidden" name="questionnaire_id" value="{{ $id }}">
		<div class="push-more-question">
			<div class="form-group">
				<label class="control-label col-sm-2" >Question Type:</label>
				<div class="col-sm-6">

					<select name="questions[0][type]" class="form-control">
						<option value="MCQs">Multipule Choice(Single option)</option>
					</select>	
				</div>
				<div class="col-md-2">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" >Question:</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" name="questions[0][question]" placeholder="Enter Question">
				</div>
			</div>
			<div class="answer-list-0">
				<div class="form-group question" id="">
					<label class="control-label col-sm-2" >Choice:</label>
					<div class="col-sm-6">
						<input class="form-control" type="text" name="questions[0][answers][0][answer]" placeholder="Enter Choice">
					</div>
					<div class="col-md-2">
						<label><input type="checkbox" name="questions[0][answers][0][status]" value="1"> Correct?</label>
					</div>
					<div class="col-md-2">
						
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-2 col-md-offset-2" >
					<span class="btn btn-primary  add-more-answer" data-ques="0" data-total="0">Add Choice</span>
				</div>
			</div>
		</div>

			<div class="form-group">
				<div class="col-md-2 col-md-offset-2 add-more-question" data-total_question="1">
					<span class="btn btn-success">Add More Question</span>
				</div>
			</div>		

			<input type="submit" value="Add to Questionnaires" class="btn btn-success" >
	{{ Form::close() }}	
</div>


@endsection