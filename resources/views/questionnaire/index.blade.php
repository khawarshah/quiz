@extends('layouts.app')
@section('content')
<div class="container" >
	<h1>Create</h1>
	<table cellpadding="0" cellspacing="0" border="0" class="dataTable" id="questionnaire_table">
	    <thead>
	        <tr>
	            <th>Id</th>
	            <th>Name</th>
	            <th>Number of Question</th>
	            <th>Duration</th>
	            <th>Resumable</th>
	            <th>Published</th>
	            <th>Action</th>
	        </tr>
	    </thead>
	    <tbody>
	       @foreach($questionnaires as $q)
	        <tr>
	            <td>{{ $q->id }}</td>
	            <td>{{ $q->name }}</td>
		        <td>{{ count($q->questions) }} | 
		        {{ Form::open(["route" => "question.add", "method" => "POST"]) }}
		        	<input type="hidden" name="id" value="{{ $q->id }}">
		            <input type="submit" value="Add">
		        {{ Form::close() }}
				</td>
	            <td>{{ $q->duration }}{{ $q->duration_in }}</td>
	            <td>{{ $q->is_resumeable }}</td>
	            <td>{{ $q->is_published }}</td>
	            <td><a href="{{ route('questionnaires.list', ['id' => $q->id]) }}" title="">View</a> | 
	            {{ Form::open(["route" => ['questionnaire', $q->id], "method" => "delete"]) }}
	            <input type="submit" value="Delete">
	            {{ Form::close() }}

	            </td>
	        </tr>
	       @endforeach         
	    </tbody>
	</table>
</div>
@endsection