@extends('layouts.app')
@section('content')
<div class="container" >
	<h1>Create</h1>
	<form class="form-horizontal" method="post" action="{{ route('questionnaires.create') }}">
{{ csrf_field() }}
		<div class="row">
			<div class="form-group col-sm-8">
			    <label class="control-label col-sm-3"  for="questionnaire_name"  style="text-align: -webkit-left;">Questionnaire Name:</label>
			    <div class="col-sm-6">
			      	<input type="text" class="form-control" name="name" id="questionnaire_name" placeholder="Enter Questionnaire Name">
			    </div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-sm-6">
			    <label class="control-label col-sm-2" for="duration">Duration:</label>
			    <div class="col-sm-5"> 
				    <input type="text" class="form-control" name="duration" id="duration" placeholder="Enter Duration">
			    </div>
			    <div class="col-sm-5"> 
			    	<select class="form-control" name="duration_in" >
				      	<option selected disabled>Select time</option>
				      	<option value="min">Minutes</option>
				      	<option value="hr">Hours</option>
			      	</select>
			    </div>			    
			</div>
		</div>
		<div class="row">
			<div class="form-group col-sm-6">
		  		<div class="row">
				    <label class="control-label col-sm-3" for="duration">Can Resume:</label>
				    <div class="col-sm-2"> 
					    <label class="control-label " for="yes_resume">Yes</label>
					    <input type="radio" name="is_resumeable" value="Yes" id="yes_resume">
				    </div>		    
				    <div class="col-sm-2"> 
					    <label class="control-label " for="no_resume">No</label>
					    <input type="radio" name="is_resumeable" value="No" id="no_resume">
				    </div>		    
		    	</div>
		    </div>
		</div>			
		<div class="row">
			<div class="form-group col-sm-6"> 
			    <div class=" pull-right">
			      	<button type="submit" class="btn btn-default">Submit</button>
			    </div>
			</div>
		</div>
	</form>
</div>
@endsection