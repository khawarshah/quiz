jQuery(document).on("click", ".add-more-answer", function(){
	var questionNo = jQuery(this).data("ques");
	var totalNoOfQues = jQuery(this).data("total");
	if (totalNoOfQues == 0) {
		totalNoOfQues = totalNoOfQues+1;
	}
	console.log(totalNoOfQues + " Before " + jQuery(this).data("total"));
	var html = '<div class="form-group question" id="question-'+questionNo+'-'+totalNoOfQues+'">\
					<label class="control-label col-sm-2" >Choice:</label>\
					<div class="col-sm-6">\
						<input class="form-control" type="text" name="questions['+questionNo+'][answers]['+totalNoOfQues+'][answer]" placeholder="Enter Choice">\
					</div>\
					<div class="col-md-2">\
						<label><input type="checkbox" value="1" name="questions['+questionNo+'][answers]['+totalNoOfQues+'][status]"> Correct?</label>\
					</div>\
					<div class="col-md-2">\
						<a class="remove-question-choice" data-id="question-'+questionNo+'-'+totalNoOfQues+'">Delete Choice</a>\
					</div>\
				</div>';



	jQuery(".answer-list-"+questionNo).append(html);
	jQuery(this).data("total", totalNoOfQues+1);
	totalNoOfQues = "";
});

jQuery(document).on("click", ".remove-question-choice", function(){
	var choiceNo = jQuery(this).data("id");
	jQuery("#"+choiceNo).remove();
});


jQuery(document).on("click", ".add-more-question", function(){
	var numOfQuestions = jQuery(this).data("total_question");

	var questionhtml = '<div id="question-'+numOfQuestions+'"><div class="form-group"><label class="control-label col-sm-2" >Question Type:</label><div class="col-sm-6"><select class="form-control" name="questions['+numOfQuestions+'][type]"><option value="MCQs">Multipule Choice(Single option)</option></select>	</div><div class="col-md-2"><a data-ques_id="question-'+numOfQuestions+'" class="remove-this-question">Delete Question</a></div></div><div class="form-group"><label class="control-label col-sm-2" >Question:</label><div class="col-sm-6"><input class="form-control" type="text" placeholder="Enter Question" name="questions['+numOfQuestions+'][question]"></div></div><div class="answer-list-'+numOfQuestions+'"><div class="form-group question" id=""><label class="control-label col-sm-2" >Choice:</label><div class="col-sm-6"><input class="form-control" type="text" name="questions['+numOfQuestions+'][answers][0][answer]" placeholder="Enter Choice"></div><div class="col-md-2"><label><input type="checkbox" value="1" name="questions['+numOfQuestions+'][answers][0][status]"> Correct?</label></div><div class="col-md-2">					</div></div></div><div class="form-group"><div class="col-md-2 col-md-offset-2 "><span class="btn btn-primary add-more-answer" data-ques="'+numOfQuestions+'" data-total="1">Add Choice</span></div></div>';

		jQuery(".push-more-question").append(questionhtml);

	jQuery(this).data("total_question", numOfQuestions+1);
});

jQuery(document).on("click", ".remove-this-question", function(){
	var rmQuestionIfo = jQuery(this).data("ques_id");
	jQuery("#"+rmQuestionIfo).remove();
});





